/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgfinal.project;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.util.Random;
/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    private Connection conn;

    @FXML
    private void handleButtonAction(ActionEvent event) {
    
            
       Set();
       int a=doinsert();
       label.setText("Set"+a+"Row");
       
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    private int doinsert(){
        int rows=0;
        PreparedStatement stmt=null;
        String insert=" INSERT INTO ANDY850914.COLLEAGUES VALUES(?,?,?,?,?,?,?) ";
        try {
            if(conn==null){
            Set();
            }
            stmt=conn.prepareStatement(insert);
            stmt.setInt(1,10);
            stmt.setString(2,"Hsiao");
            stmt.setString(3,"Wen");
            stmt.setString(4,"Engineer");
            stmt.setString(5,"Engineering");
            stmt.setString(6,"andy850914@gmail.com");
            stmt.setInt(7,0);
            rows=stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }finally
        {
            try {
                stmt.close();
                conn.close();
               
                
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
               return rows;
    }

     private int doselect(){
        int rows=0;
        PreparedStatement stmt=null;
        ResultSet result=null;
        String select=" Select * From ANDY850914.COLLEAGUES ";
        try {
            if(conn==null){
            Set();
            }
            stmt=conn.prepareStatement(select);
            result=stmt.executeQuery();
            while(result.next())
            {
                System.out.println(result.getString("FIRSTNAME"));
                rows++;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }finally
        {
            try {
                result.close();
                stmt.close();
                conn.close();
               
                
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
               return rows;
    }    
    
    private void Set()
   {
        try {
            conn= DriverManager.getConnection("jdbc:derby://localhost:1527/contact","andy850914","qaz7418564000");
            if(conn!=null)
            {
               label.setText("linked");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("stupid");
        }
   }
}
